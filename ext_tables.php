<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'HIVE.HiveExtWkhtmltopdf',
            'Hiveextwkhtmltopdfrenderpdf',
            'hive_ext_wkhtmltopdf :: RenderPdf'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('hive_ext_wkhtmltopdf', 'Configuration/TypoScript', 'hive_ext_wkhtmltopdf');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hiveextwkhtmltopdf_domain_model_pdf', 'EXT:hive_ext_wkhtmltopdf/Resources/Private/Language/locallang_csh_tx_hiveextwkhtmltopdf_domain_model_pdf.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hiveextwkhtmltopdf_domain_model_pdf');

    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder