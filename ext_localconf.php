<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'HIVE.HiveExtWkhtmltopdf',
            'Hiveextwkhtmltopdfrenderpdf',
            [
                'Pdf' => 'renderPdf'
            ],
            // non-cacheable actions
            [
                'Pdf' => ''
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    hiveextwkhtmltopdfrenderpdf {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('hive_ext_wkhtmltopdf') . 'Resources/Public/Icons/user_plugin_hiveextwkhtmltopdfrenderpdf.svg
                        title = LLL:EXT:hive_ext_wkhtmltopdf/Resources/Private/Language/locallang_db.xlf:tx_hive_ext_wkhtmltopdf_domain_model_hiveextwkhtmltopdfrenderpdf
                        description = LLL:EXT:hive_ext_wkhtmltopdf/Resources/Private/Language/locallang_db.xlf:tx_hive_ext_wkhtmltopdf_domain_model_hiveextwkhtmltopdfrenderpdf.description
                        tt_content_defValues {
                            CType = list
                            list_type = hiveextwkhtmltopdf_hiveextwkhtmltopdfrenderpdf
                        }
                    }
                }
                show = *
            }
       }'
    );
    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder