
plugin.tx_hiveextwkhtmltopdf_hiveextwkhtmltopdfrenderpdf {
    view {
        # cat=plugin.tx_hiveextwkhtmltopdf_hiveextwkhtmltopdfrenderpdf/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:hive_ext_wkhtmltopdf/Resources/Private/Templates/
        # cat=plugin.tx_hiveextwkhtmltopdf_hiveextwkhtmltopdfrenderpdf/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:hive_ext_wkhtmltopdf/Resources/Private/Partials/
        # cat=plugin.tx_hiveextwkhtmltopdf_hiveextwkhtmltopdfrenderpdf/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:hive_ext_wkhtmltopdf/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_hiveextwkhtmltopdf_hiveextwkhtmltopdfrenderpdf//a; type=string; label=Default storage PID
        storagePid =
    }
}
