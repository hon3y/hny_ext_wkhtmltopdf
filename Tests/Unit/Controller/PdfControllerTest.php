<?php
namespace HIVE\HiveExtWkhtmltopdf\Tests\Unit\Controller;

/**
 * Test case.
 */
class PdfControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \HIVE\HiveExtWkhtmltopdf\Controller\PdfController
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\HIVE\HiveExtWkhtmltopdf\Controller\PdfController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

}
