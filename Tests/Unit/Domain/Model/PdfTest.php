<?php
namespace HIVE\HiveExtWkhtmltopdf\Tests\Unit\Domain\Model;

/**
 * Test case.
 */
class PdfTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \HIVE\HiveExtWkhtmltopdf\Domain\Model\Pdf
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \HIVE\HiveExtWkhtmltopdf\Domain\Model\Pdf();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function dummyTestToNotLeaveThisFileEmpty()
    {
        self::markTestIncomplete();
    }
}
