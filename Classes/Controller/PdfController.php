<?php
namespace HIVE\HiveExtWkhtmltopdf\Controller;

/***
 *
 * This file is part of the "hive_ext_wkhtmltopdf" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * PdfController
 */
class PdfController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * pdfRepository
     *
     * @var \HIVE\HiveExtWkhtmltopdf\Domain\Repository\PdfRepository
     * @inject
     */
    protected $pdfRepository = null;

    /**
     * action renderPdf
     *
     * @return void
     */
    public function renderPdfAction()
    {

    }
}
