<?php
namespace HIVE\HiveExtWkhtmltopdf\Domain\Model;

/***
 *
 * This file is part of the "hive_ext_wkhtmltopdf" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * Pdf
 */
class Pdf extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    }
